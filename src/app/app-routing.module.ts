import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactUsComponent } from './pages/content/contact-us/contact-us.component';
import { ContentComponent } from './pages/content/content.component';
import { DetailEmployeeComponent } from './pages/content/detail-employee/detail-employee.component';
import { HomeComponent } from './pages/content/home/home.component';
import { UserComponent } from './pages/content/user/user.component';

const routes: Routes = [
  {
    path: '',
    component: ContentComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'detail-employee/:nik/:name/:position/:salary',
        component: DetailEmployeeComponent
      },
      {
        path: 'detail-employee-single-id/:nik',
        component: DetailEmployeeComponent
      },
      {
        path: 'detail-employee-query-params/:nik',
        component: DetailEmployeeComponent
      },
      {
        path: 'contact-us',
        component: ContactUsComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
      },
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
