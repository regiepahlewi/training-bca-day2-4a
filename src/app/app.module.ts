import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/header/header.component';
import { ContentComponent } from './pages/content/content.component';
import { FooterComponent } from './pages/footer/footer.component';
import { DetailEmployeeComponent } from './pages/content/detail-employee/detail-employee.component';
import { HomeComponent } from './pages/content/home/home.component';
import { ContactUsComponent } from './pages/content/contact-us/contact-us.component';
import { EmployeeService } from './services/employee.service';
import { HttpClientModule } from '@angular/common/http';
import { ExperienceLevelPipe } from './pipe/experience-level.pipe';
import { ResultAdministrationPipe } from './pipe/result-administration.pipe';
import { UserComponent } from './pages/content/user/user.component';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    DetailEmployeeComponent,
    HomeComponent,
    ContactUsComponent,
    ExperienceLevelPipe,
    ResultAdministrationPipe,
    UserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    EmployeeService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
