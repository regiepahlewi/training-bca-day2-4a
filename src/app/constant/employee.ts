import { IEmployeeLevel } from "../interfaces/employee";

export const EMPLOYEE_LEVEL: IEmployeeLevel[] = [
    { experience: '<1 Years', level: 'Junior Staff', expected: 6000000 },
    { experience: '>1 Years', level: 'Junior Staff', expected: 6000000 },
    { experience: '>2 Years', level: 'Junior Staff', expected: 9000000 },
    { experience: '2 Years', level: 'Junior Staff', expected: 9000000 },
    { experience: '<3 Years', level: 'Junior Staff', expected: 9000000 },
    { experience: '>3 Years', level: 'Middle Staff', expected: 10000000 },
    { experience: '>5 Years', level: 'Senior Staff', expected: 12000000 },
    { experience: '>6 Years', level: 'Senior II Staff', expected: 12000000 }
]