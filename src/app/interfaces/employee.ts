export interface IEmployee {
    nik: string;
    name: string;
    position: string;
    salary: string;
    email?: string;
    birthDate?: string;
    birthPlace?: string;
    experience: string;
    gender?: string;
    location?: string;
    phone?: number;
    address?: string;   
    experienceWithExpected: string;
}

export interface IEmployeeLevel {
    experience: string;
    level: string;
    expected: number;
}
