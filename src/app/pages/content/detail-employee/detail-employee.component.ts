import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEmployee } from 'src/app/interfaces/employee';
@Component({
  selector: 'app-detail-employee',
  templateUrl: './detail-employee.component.html',
  styleUrls: ['./detail-employee.component.scss']
})
export class DetailEmployeeComponent implements OnInit {

  data: IEmployee = {
    nik: '',
    name: '',
    position: '',
    salary: '',
    experience: '',
    experienceWithExpected: ''
  };
  constructor(
    private _activeRoute: ActivatedRoute
  ) {  }

  ngOnInit(): void {
    // Routing 1
    //this.getDetail();

    // Routing 2
    this.getDetailFromLocalStorage();

    // Routing 3
    // this.getDetailQueryParams();
  }

  // Routing 1
  getDetail(): void {
    this.data = {
      nik: this._activeRoute.snapshot.paramMap.get('nik')!,
      name: this._activeRoute.snapshot.paramMap.get('name')!,
      position: this._activeRoute.snapshot.paramMap.get('position')!,
      salary: this._activeRoute.snapshot.paramMap.get('salary'),
      experience: '>1 Years',
      experienceWithExpected: `>1 Years:${this._activeRoute.snapshot.paramMap.get('salary')}`
    }
  }

  // Routing 2
  getDetailFromLocalStorage(): void {
    const nik = this._activeRoute.snapshot.paramMap.get('nik')!;
    this.getData(nik);
  }

  // Routing 2
  getData(nik: string): void {
    const listCandidates: IEmployee[] = JSON.parse(localStorage.getItem('listCandidates'));
    this.data = listCandidates.filter(a => a.nik === nik)[0];
    this.data.experienceWithExpected = `${this.data.experience}:${this.data.salary}`;
  }

  // Routing 3
  getDetailQueryParams(): void {
    this._activeRoute.queryParamMap.subscribe((params: any) => {
      const nik = params.params.nik;
      this.getData(nik);
    });
  }

}
