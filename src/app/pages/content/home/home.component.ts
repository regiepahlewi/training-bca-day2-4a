import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private _router : Router,
    private _employeeService: EmployeeService
  ) { }

  listCandidates: any[] = [];

  candidate: any;
  province: any = ['DKI Jakarta', 'Jawa Barat', 'Jawa Tengah'];
  jobsRole: any = ['IT', 'Finance', 'Human Resource'];
  jobExperience: any = ['<1 Years','>1 Years','>2 Years','2 Years','3 Years','>3 Years','>6 Years'];
  showError = false;
  state: number = 0;

  jobForm = this.fb.group({
    name: [null, Validators.required],
    nik: [null, [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
    phone: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    birthPlace: [null, Validators.required],
    birthDate: [null, Validators.required],
    gender: [null, Validators.required],
    address: [null, Validators.required],
    position: [null, Validators.required],
    experience: [null, Validators.required],
    location: [null, Validators.required],
    salary: [null, Validators.required],
    term: [false, Validators.required],
  });

  ngOnInit(): void {
    // Routing
    this.loadData();
  }

  onSubmit(): void {

    // if (!this.listCandidates.some((el: { nik: null | undefined }) => el.nik === this.jobForm.get('nik')?.value)) {
    //   this.showError = false;
    // } else {
    //   this.showError = true;
    // }

    if (this.state === 0) {
      this.listCandidates.push(this.jobForm.value);
    } else {
      const idx = this.listCandidates.findIndex(a => a.nik === this.jobForm.get('nik')?.value);
      this.listCandidates[idx] = this.jobForm.value;
    }

    // DI
    this._employeeService.activeSelectedEmployeeName = this.jobForm.getRawValue().name!;
    
    // Routing 2
    this.onSuccessInsert();
    this.jobForm.reset();
    this.state = 0;
    
  }

  // Routing 1
  loadData(): void {
    this.listCandidates = JSON.parse(localStorage.getItem('listCandidates')!);
    if(!this.listCandidates) {
      this.listCandidates = [];
    }
    console.log(this.listCandidates);
  }

  onEdit(nik: string): void {
    const data = this.listCandidates.filter(a => a.nik === nik)[0];
    this.jobForm.patchValue({
      name: data.name,
      nik: data.nik,
      phone: data.phone,
      email: data.email,
      birthPlace: data.birthPlace,
      birthDate: data.birthDate,
      gender: data.gender,
      address: data.address,
      position: data.position,
      experience: data.experience,
      location: data.location,
      salary: data.salary,
      term: data.term
    })
    this.state = 1;
  }

  onDelete(nik: string): void {
    const idx = this.listCandidates.findIndex(a => a.nik === nik);
    this.listCandidates.splice(idx, 1);
    this.jobForm.reset();
    this.onSuccessInsert();
  }

  // Routing 3
  onDetail(nik: string): void {
    this._router.navigate(['detail-employee-single-id', nik]);
  }

  onSuccessInsert(): void {
    localStorage.setItem('listCandidates', JSON.stringify(this.listCandidates));
    this.loadData();
  }

}
