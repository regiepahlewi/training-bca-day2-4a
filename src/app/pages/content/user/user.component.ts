import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUser } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  listUsers: IUser[] = [];
  userForm!: FormGroup;
  state: number = 0;

  constructor(
    private _fb: FormBuilder,
    private _userService: UserService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.createForm();
  }

  loadData(): void {
    this._userService.getUsers().subscribe(data => {
      this.listUsers = data;
      this.state = 0;
      this.userForm.reset();
    })
  }

  createForm(): void {
    this.userForm = this._fb.group({
      id: [null],
      name: ['', Validators.required],
      avatar: ['', Validators.required],
      createdAt: [null]
    });
  }

  onSubmit(): void {
    const payload: IUser = {
      id: (this.state === 0 ) ? undefined : this.userForm.getRawValue().id,
      name: this.userForm.getRawValue().name,
      avatar: this.userForm.getRawValue().avatar
    }

    if (this.state === 0) {
      this._userService.addUser(payload).subscribe(_ => {
        this.loadData();
      })
    } else {
      this._userService.updateUser(payload).subscribe(_ => {
        this.loadData();
      })
    }
  }

  onDelete(id: string): void {
    this._userService.deleteUser(id).subscribe(_ => {
      this.loadData();
    })
  }

  onEdit(id: string): void {
    this._userService.getUser(id).subscribe(data => {
      this.userForm.patchValue({
        id: data.id,
        name: data.name,
        avatar: data.avatar,
        createdAt: data.createdAt
      })
      this.state = 1;
    })
  }
}
