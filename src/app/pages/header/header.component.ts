import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menus: any;

  constructor(
    // DI 1
    private _employeeService: EmployeeService
  ) { }

  ngOnInit(): void {

    this.menus = [
      {
        id: 1,
        name: 'HOME',
        routerLink: '',
        params: { }
      },
      // {
      //   id: 2,
      //   name: 'DETAIL EMPLOYEE WITH STATE PARAMS',
      //   routerLink: 'detail-employee/1111111111111111/Regie Pahlewi/IT/10000000',
      //   params: { nik : '1233333222221111' }
      // },
      // {
      //   id: 3,
      //   name: 'DETAIL EMPLOYEE WITH QUERY PARAMS',
      //   routerLink: 'detail-employee-query-params',
      //   params: { nik : '1233333222221111' }
      // },
      // {
      //   id: 4,
      //   name: 'CONTACT US',
      //   routerLink: 'contact-us',
      //   params: { }
      // },
      {
        id: 5,
        name: 'User',
        routerLink: 'user',
        params: { }
      }
    ];

    // DI 2
    this._employeeService.activeSelectedEmployeeName = 'Agus';

  }

  // DI 3
  ngDoCheck(): void {
    console.log('selected employee name', this._employeeService.activeSelectedEmployeeName);
  }

}
