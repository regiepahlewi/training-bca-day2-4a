import { Pipe, PipeTransform } from '@angular/core';
import { EMPLOYEE_LEVEL } from '../constant/employee';
import { IEmployeeLevel } from '../interfaces/employee';

@Pipe({
  name: 'experienceLevel'
})
export class ExperienceLevelPipe implements PipeTransform {

  transform(value: string): string {
    const experience: IEmployeeLevel[] = EMPLOYEE_LEVEL;
    const employeeLevel = experience.filter(a => a.experience === value);
    return (employeeLevel.length > 0) ? employeeLevel[0].level : ''
  }

}
