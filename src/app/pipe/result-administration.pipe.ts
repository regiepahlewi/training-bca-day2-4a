import { Pipe, PipeTransform } from '@angular/core';
import { EMPLOYEE_LEVEL } from '../constant/employee';
import { IEmployeeLevel } from '../interfaces/employee';

@Pipe({
  name: 'resultAdministration'
})
export class ResultAdministrationPipe implements PipeTransform {

  transform(value: string): string {
    const strSplit = value.split(':');
    const experience = strSplit[0];
    const expectedSalary = parseFloat(strSplit[1]);

    const experienceArr: IEmployeeLevel[] = EMPLOYEE_LEVEL;
    const employeeLevel = experienceArr.filter(a => a.experience === experience && a.expected > expectedSalary);
    
    const res = (employeeLevel.length > 0) ? 'Passed Administration Test' : 'Not Passed';
    return res;
  }

}
