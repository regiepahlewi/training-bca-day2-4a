import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private http: HttpClient
  ) { }

  activeSelectedEmployeeName: string = '';

  getGithubRepo(): Observable<any> {
    return this.http.get('https://api.github.com/users/regiepahlewi/repos');
  }
}
