import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private _http: HttpClient
  ) { }

  private _apiUrl : string = `${environment.apiUrl}/users`;

  getUsers(): Observable<IUser[]> {
    return this._http.get<IUser[]>(this._apiUrl);
  }

  getUser(id: string): Observable<IUser> {
    return this._http.get<IUser>(`${this._apiUrl}/${id}`);
  }

  addUser(data: IUser): Observable<IUser> {
    return this._http.post<IUser>(this._apiUrl, data);
  }

  updateUser(data: IUser): Observable<IUser> {
    return this._http.put<IUser>(`${this._apiUrl}/${data.id}`, data);
  }

  deleteUser(id: string): Observable<IUser> {
    return this._http.delete<IUser>(`${this._apiUrl}/${id}`);
  }

}
